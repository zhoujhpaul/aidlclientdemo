// BookManager.aidl
package com.zjhtest.aidlclient;

import com.zjhtest.aidlclient.Book;

interface BookManager {
    List<Book> getBooks();
    void addBook(in Book book);
}
