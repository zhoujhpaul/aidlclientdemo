package com.zjhtest.aidlclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity Client";
    private static final String AIDL_SERVER_PACKAGE = "com.zjhtest.aidlserver";  // 跨进程调用目标进程的唯一包名
    private static final String AIDL_SERVER_SERVICE_ACTION = "com.zjhtest.aidlserver.service";  // 跨进程调用目标进程的服务action

    // 由AIDL文件生成的BookManager接口
    private BookManager mBookManager;
    private boolean mBound = false;
    private List<Book> mBooks;
    private Button mAddBookBtn;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "client: service connected");
            mBound = true;
            mBookManager = BookManager.Stub.asInterface(iBinder);

            if (mBookManager != null) {
                try {
                    mBooks = mBookManager.getBooks();
                    Log.d(TAG, "client: get books is " + mBooks);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "client: service disconnected");
            mBound = false;
        }
    };

    private void addBook() {
        if (!mBound) {
            attemptToBindService();
            Toast.makeText(this, "当前与服务端处于未连接状态，正在尝试重连，请稍后再试", Toast.LENGTH_SHORT).show();
            return ;
        }

        if (mBookManager == null) {
            return ;
        }

        Book book = new Book("Android权威编程指南", 98);
        try {
            mBookManager.addBook(book);  // 跨进程调用com.zjhtest.aidlserver应用的addBook方法
            Log.d(TAG, "client: newBook is " + book);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void attemptToBindService() {
        Intent intent = new Intent();
        intent.setPackage(AIDL_SERVER_PACKAGE);
        intent.setAction(AIDL_SERVER_SERVICE_ACTION);
        bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAddBookBtn = findViewById(R.id.addBookBtn);
        mAddBookBtn.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mBound) {
            Log.d(TAG, "attemptToBindService execute");
            attemptToBindService();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addBookBtn:
                addBook();
        }
    }
}
